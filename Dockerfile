FROM python:3.8-slim

WORKDIR /app

COPY src/test-requirements.txt /tmp/test-requirements.txt

COPY src/requirements.txt /tmp/requirements.txt

RUN pip install -r /tmp/test-requirements.txt \
    && pip install -r /tmp/requirements.txt

COPY src .
 
ENTRYPOINT ["/app/docker-entrypoint.sh"]
