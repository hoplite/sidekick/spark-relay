import logging

from webexteamssdk import WebexTeamsAPI
from webexteamssdk.exceptions import ApiError

LOG = logging.getLogger(__name__)


class WebexTeamsClient(object):
    """
    A Spark client class
    """

    def __init__(self, config):
        self.access_token = config.get('WEBEX_TEAMS_ACCESS_TOKEN')
        self.room_id = None
        self.api = WebexTeamsAPI(access_token=self.access_token,
                                 wait_on_rate_limit=False)
        self.person_id = None

    def _have_room_access(self, room_id):
        """Given a Spark room id, check to see if we have access to it."""

        try:
            room_ids = [room.id for room in self.api.rooms.list()]
            return room_id in room_ids

        except ApiError as e:
            LOG.error(f'Room access check failed {e}')
            return False

    def _get_room_id(self, room_name):
        """
        Given a Spark room name, set the room_id class variable
        """

        try:
            rooms = self.api.rooms.list()
            for room in rooms:

                if room_name == room.title:
                    LOG.debug(f'Setting room id to {room.id} '
                              f'based on {room_name}')
                    return room.id
        except ApiError as e:
            LOG.error(f'get_room failed {e}')

        return None

    def set_person_id(self, email):
        """
        Given the email of a Spark user, set the person_id class variable

        :param email (str): The email address of the person to search for.
        """

        try:
            # For some reason, we can only pull people by email
            # if we use the list method. It will return a list
            # with one entry. Lol
            people = self.api.people.list(email=email)
            for person in people:
                if email in person.emails:
                    self.person_id = person.id
                    break
        except ApiError as e:
            LOG.error(f'get_person failed {e}')

    def send_message(self, message_text=None, markdown_text=None,
                     room_id=None, room_name=None):
        """
        Given message text and a room or a person id, send a message to Spark.

        :param message(str): The message text that needs to be sent
        :param room (boolean): Whether to send the message to a spark room.
        """

        message_args = {'roomId': None,
                        'toPersonId': None,
                        'text': ''}
        if room_name:
            room_id = self._get_room_id(room_name)

        if room_id:
            LOG.info(f'Room ID {room_id}')
            if self._have_room_access(room_id):
                LOG.info(f'We have access to room {room_id}')
                message_args['roomId'] = room_id
                try:
                    message_args['text'] = message_text
                    message_args['markdown'] = markdown_text
                    LOG.debug(f'person_id {self.person_id}')
                    LOG.debug(f'message_args {message_args}')
                    message = self.api.messages.create(**message_args)
                    LOG.info(f'Sent message: {message} to room {room_id}')
                except ApiError as e:
                    LOG.error(f'send_message Failed {e}')
        else:
            LOG.info(f'We do not have room access to {room_id}.'
                     f' Dropping the message')
            return
