import logging
import os
import sys

# Local imports
from webex_teams_relay.consumer import MessageConsumer
from instance.config import app_config

log_level = logging.INFO
format = '%(asctime)s - %(levelname)s - %(message)s'
logging.basicConfig(format=format, datefmt='%m-%d %H:%M', level=log_level)
LOG = logging.getLogger(__name__)

try:
    config_class = app_config[os.getenv('APP_SETTINGS')]
except KeyError as e:
    LOG.error('Please supply the APP_SETTINGS bash environment variable')
    sys.exit(1)

def config_from_object(obj):
    """
    This function receives a config object, and returns a config dictionary.
    It was taken from the flask project to replicate their config handling.

    :param obj: Object class passed in with config data
    :return config: Dictionary of configuration settings
    """

    config = {}
    for key in dir(obj):
        if key.isupper():
            config[key] = getattr(obj, key)

    return config


def check_messages():
    """
    Instantiate a MessageConsumer class and begin checking for new messages
    """

    config = config_from_object(config_class)
    LOG.debug(f'config {config}')

    try:
        message_consumer = MessageConsumer(config)
        message_consumer.relay_messages()
    except Exception as e:
        LOG.error('Error is {0}'.format(e))


if __name__ == '__main__':
    check_messages()
