import json
import mock
import pika
from webex_teams_relay.consumer import MessageConsumer


class TestConsumer(object):

    @mock.patch('webex_teams_relay.consumer.WebexTeamsClient')
    def test_on_message(self, mock_webex_teams):
        config = {'AMQP_USER': 'guest',
                  'AMQP_PASS': 'guest',
                  'AMQP_HOST': 'localhost'}
        mock_chan = mock.Mock()
        mock_method_frame = mock.MagicMock()

        mock_method_frame.return_value = pika.spec.Basic.Deliver(
            'consumer_tag=ctag1.e58bd333eb824ca9a53c9a4979d8348f',
            'delivery_tag=1', 'exchange=', 'redelivered=False',
            'routing_key=webex_teams')
        m = MessageConsumer(config)
        body = dict(messagetext='blah', roomid='test123')
        m._on_message(mock_chan, mock_method_frame, None, json.dumps(body))

        mock_webex_teams().send_message.assert_called_with(
            message_text=body['messagetext'],
            markdown_text=None,
            room_name=body['roomid'],
        )

    @mock.patch('webex_teams_relay.consumer.WebexTeamsClient')
    def test_on_message_markdown(self, mock_webex_teams):
        config = {'AMQP_USER': 'guest',
                  'AMQP_PASS': 'guest',
                  'AMQP_HOST': 'localhost'}
        mock_chan = mock.Mock()
        mock_method_frame = mock.MagicMock()

        mock_method_frame.return_value = pika.spec.Basic.Deliver(
            'consumer_tag=ctag1.e58bd333eb824ca9a53c9a4979d8348f',
            'delivery_tag=1', 'exchange=', 'redelivered=False',
            'routing_key=webex_teams')
        m = MessageConsumer(config)
        body = dict(markdowntext='**blah** markdown message', roomid='test123')
        m._on_message(mock_chan, mock_method_frame, None, json.dumps(body))

        mock_webex_teams().send_message.assert_called_with(
            message_text=None,
            markdown_text=body['markdowntext'],
            room_name=body['roomid'],
        )

    @mock.patch('webex_teams_relay.consumer.WebexTeamsClient')
    def test_on_message_no_method_frame(self, mock_webex_teams):
        config = {'AMQP_USER': 'guest',
                  'AMQP_PASS': 'guest',
                  'AMQP_HOST': 'localhost'}
        mock_chan = mock.Mock()

        m = MessageConsumer(config)
        m._on_message(channel=mock_chan, method_frame=None, header_frame=None,
                      body=None)

        mock_webex_teams().send_message.assert_not_called

    @mock.patch('webex_teams_relay.consumer.WebexTeamsClient')
    @mock.patch('webex_teams_relay.consumer.pika')
    def test_consume_message(self, mock_pika, mock_webex_teams):
        config = {'AMQP_USER': 'guest',
                  'AMQP_PASS': 'guest',
                  'AMQP_HOST': 'localhost'}

        m = MessageConsumer(config)
        m.relay_messages()
