import json
import mock
from webexteamssdk.models.immutable import Person, Room
from webexteamssdk.generator_containers import GeneratorContainer

from webex_teams_relay.webex_teams import WebexTeamsClient


class TestWebexTeamsClient(object):

    @mock.patch('webex_teams_relay.webex_teams.WebexTeamsAPI')
    def test_send_message_with_room_id(self, mock_api):
        """Given a room ID test that we can send messages to the Webex
           teams API correctly."""

        def room_list():
            room_data = {"id": "xyz987", "title": "test-room", "type": "group", "isLocked": False,
                         "lastActivity": "2018-10-31T02:44:53.390Z",
                         "teamId": "def345", "creatorId": "ghi678",
                         "created": "2018-10-31T02:44:33.188Z"}
            room = Room(json_data=json.dumps(room_data))
            yield room

        config = {'WEBEX_TEAMS_ACCESS_TOKEN': 'abc123456',
                  'DEBUG': True,
                  'TESTING': False,
                  'AMQP_HOST': 'localhost',
                  'AMQP_USER': 'guest',
                  'AMQP_PASS': 'guest'}
        webex_teams_message_args = {'roomId': 'xyz987',
                              'toPersonId': None,
                              'text': 'Hello World',
                              'markdown': None}

        room_generator_container = GeneratorContainer(room_list)
        mock_api().rooms.list.return_value = room_generator_container

        c = WebexTeamsClient(config)
        c.send_message(message_text='Hello World',
                       room_id='xyz987')

        mock_api().messages.create.assert_called_with(**webex_teams_message_args)

    @mock.patch('webex_teams_relay.webex_teams.WebexTeamsAPI')
    def test_send_message_with_room_name(self, mock_api):
        """Given a room name test that we can send messages to the Webex
           teams API correctly."""

        def room_list():
            room_data = {"id": "xyz987", "title": "test-room", "type": "group", "isLocked": False,
                         "lastActivity": "2018-10-31T02:44:53.390Z",
                         "teamId": "def345", "creatorId": "ghi678",
                         "created": "2018-10-31T02:44:33.188Z"}
            room = Room(json_data=json.dumps(room_data))
            yield room

        config = {'WEBEX_TEAMS_ACCESS_TOKEN': 'abc123456',
                  'DEBUG': True,
                  'TESTING': False,
                  'AMQP_HOST': 'localhost',
                  'AMQP_USER': 'guest',
                  'AMQP_PASS': 'guest'}
        webex_teams_message_args = {'roomId': 'xyz987',
                              'toPersonId': None,
                              'text': 'Hello World',
                              'markdown': None}

        room_generator_container = GeneratorContainer(room_list)
        mock_api().rooms.list.return_value = room_generator_container

        c = WebexTeamsClient(config)
        c.send_message(message_text='Hello World',
                       room_name='test-room')

        mock_api().messages.create.assert_called_with(**webex_teams_message_args)

    @mock.patch('webex_teams_relay.webex_teams.WebexTeamsAPI')
    def test_send_markdown_message_with_room_name(self, mock_api):
        """Given a room name test that we can send messages to the Webex
           teams API correctly."""

        def room_list():
            room_data = {"id": "xyz987", "title": "test-room", "type": "group", "isLocked": False,
                         "lastActivity": "2018-10-31T02:44:53.390Z",
                         "teamId": "def345", "creatorId": "ghi678",
                         "created": "2018-10-31T02:44:33.188Z"}
            room = Room(json_data=json.dumps(room_data))
            yield room

        config = {'WEBEX_TEAMS_ACCESS_TOKEN': 'abc123456',
                  'DEBUG': True,
                  'TESTING': False,
                  'AMQP_HOST': 'localhost',
                  'AMQP_USER': 'guest',
                  'AMQP_PASS': 'guest'}
        webex_teams_message_args = {'roomId': 'xyz987',
                              'toPersonId': None,
                              'text': None,
                              'markdown': '**Hello World**'}

        room_generator_container = GeneratorContainer(room_list)
        mock_api().rooms.list.return_value = room_generator_container

        c = WebexTeamsClient(config)
        c.send_message(markdown_text='**Hello World**',
                       room_name='test-room')

        mock_api().messages.create.assert_called_with(**webex_teams_message_args)


    @mock.patch('webex_teams_relay.webex_teams.WebexTeamsAPI')
    def test_get_room_id(self, mock_api):
        """Given a room name, ensure that a room ID is returned"""

        def room_list():
            room_data = {"id": "abc123", "title": "test-room", "type": "group", "isLocked": False,
                         "lastActivity": "2018-10-31T02:44:53.390Z",
                         "teamId": "def345", "creatorId": "ghi678",
                         "created": "2018-10-31T02:44:33.188Z"}
            room = Room(json_data=json.dumps(room_data))
            yield room

        config = {'WEBEX_TEAMS_ACCESS_TOKEN': 'abc123456',
                  'WEBEX_TEAMS_CLIENT_ROOM': 'test-room',
                  'DEBUG': True,
                  'TESTING': False,
                  'AMQP_HOST': 'localhost',
                  'AMQP_USER': 'guest',
                  'AMQP_PASS': 'guest'}

        room_generator_container = GeneratorContainer(room_list)
        mock_api().rooms.list.return_value = room_generator_container

        c = WebexTeamsClient(config)
        room_id = c._get_room_id('test-room')

        assert room_id == "abc123"

    @mock.patch('webex_teams_relay.webex_teams.WebexTeamsAPI')
    def test_set_person_id(self, mock_api):
        """Given an email address ensure that a person ID is returned"""

        def people_list():
            person_data = {
                "id": "abc123",
                "emails": [
                    "daspano@cisco.com"
                ],
                "displayName": "Dave Spano",
                "nickName": "Dave",
                "firstName": "Dave",
                "lastName": "Spano",
                "avatar": "https://1efa7a94ed216783e352-c62266528714497a17239ececf39e9e2.ssl.cf1.rackcdn.com/V1~70417c0dc55044b0ffeafe00d6c0b070~uak1PFzdStOqHIytI0M75g==~1600",
                "orgId": "def345",
                "created": "2014-10-04T21:19:17.680Z",
                "status": "unknown",
                "type": "person"
            }
            person = Person(json_data=json.dumps(person_data))

            yield person

        config = {'WEBEX_TEAMS_ACCESS_TOKEN': 'abc123456',
                  'DEBUG': True,
                  'TESTING': False,
                  'AMQP_HOST': 'localhost',
                  'AMQP_USER': 'guest',
                  'AMQP_PASS': 'guest'}

        person_generator_container = GeneratorContainer(people_list)
        mock_api().people.list.return_value = person_generator_container

        c = WebexTeamsClient(config)
        c.set_person_id('daspano@cisco.com')


        assert c.person_id == "abc123"
