import os


class Config(object):
    """Parent configuration class."""
    DEBUG = False
    WEBEX_TEAMS_ACCESS_TOKEN = os.environ.get('WEBEX_TEAMS_ACCESS_TOKEN')
    AMQP_HOST = os.environ.get('AMQP_HOST')
    AMQP_USER = os.environ.get('AMQP_USER')
    AMQP_PASS = os.environ.get('AMQP_PASS')


class DevelopmentConfig(Config):
    """Configurations for Development."""

    DEBUG = True
    TESTING = False

class ProductionConfig(Config):
    """Configurations for Production."""

    DEBUG = False
    TESTING = False

app_config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
}
